import styled from 'styled-components';

export const LandingPageContainer = styled.div`

  font-family: 'Proxima Nova', sans-serif;

  .landingCont {
    display: flex;
    padding: 6rem 4rem 5rem 4rem;
  }

  .leftHand {
    display: flex;
    flex-direction: column;
    color: white;
    background: #222222;
    height: 25rem;
    width: 100%;
    margin-right: 5rem;
  }

  .leftCont {
    padding: 9rem 0 0 0;
    margin-left: 1.5rem;
    font-size: 2rem;
  }

  h1 {
    margin: 0;
  }

  .oprec-text {
    font-weight: 1000;
  }

  .rightHand {
    display: flex;
    flex-direction: column;
    width: 100%;
    background: white;
    color: #589F97;
  }

  .rightCont {
    padding: 12.5rem 0 0rem 0;
    font-size: 2rem;
  }

  h1.sso span {
    border-bottom: 2px solid #589F97;
    cursor: pointer;
    font-weight: 1000;
  }

  .sso2 {
    margin-top: 2.5rem;
  }

  h1.sso2 span {
    border-bottom: 2px solid #589F97;
    cursor: pointer;
    font-weight: 1000;
  }

  @media(max-width:1280px) {
    .leftCont, .rightCont {
      font-size: 1.6rem;
    }
  }

  @media(max-width:1024px) {
    .landingCont {
      padding: 6rem 2rem 5rem 2rem;
      flex-direction: column;
    }

    .rightCont {
      padding-top: 5rem;
    }
  }

  @media(max-width:580px) {
    .landingCont {
      padding: 3rem 1rem 5rem 1rem;
    }
    .leftCont, .rightCont {
      font-size: 1.2rem;
    }
    .leftHand {
      height: 20rem;
    }
  }

  @media(max-width:480px) {
    .landingCont {
      padding: 2rem 1rem 5rem 1rem;
    }
    .leftCont, .rightCont {
      font-size: 1rem;
    }

    .leftHand {
      height: 16rem;
    }

    .rightCont {
      padding-top: 3rem;
    }

    .leftCont {
      padding: 7rem 0 0 0;
    }
    .sso2 {
      margin-top: 1.5rem;
    }
  }

`;
