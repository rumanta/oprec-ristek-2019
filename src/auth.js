/* eslint-disable sort-keys, valid-jsdoc */

import axios from 'axios';

const auth = {

  /**
  * Logs a user in, returning a promise with `true` when done
  * @param  {string} email The email of the user
  * @param  {string} password The password of the user
  */
  login(user) {
    if (auth.loggedIn()) {
      return Promise.resolve(true)
    }

    localStorage.oprec2019 = JSON.stringify(user);

    return Promise.resolve(true);
  },

  /**
  * Logs the current user out
  */
  logout() {
    localStorage.removeItem('oprec2019');
    Reflect.deleteProperty(axios.defaults.headers.common, 'Authorization');
  },

  /**
  * Checks if a user is logged in
  */
  loggedIn() {
    const isUserExist = Boolean(localStorage.oprec2019);

    return isUserExist ? JSON.parse(localStorage.oprec2019) : false;
  }
};

export default auth;
