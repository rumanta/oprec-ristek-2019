import auth from './auth';
import axios from 'axios';

// const API_PREFIX_URL = "http://localhost:8000/api";
const API_PREFIX_URL = "https://ristek.cs.ui.ac.id/oprec-api/api";

axios.interceptors.request.use(
  config => {
    const user = auth.loggedIn();
    if (user) {
      const token = user.token;

      if (token) {
        config.headers.Authorization = `Token ${token}`;
      }
    }

    return config;
  },
  error => Promise.reject(error)
);

const userAuth = auth.loggedIn();

axios.defaults.headers.common.Authorization = `JWT ${userAuth.token}`;

// export const loginApi = `http://localhost:8000/login/`;
export const loginApi = `https://ristek.cs.ui.ac.id/oprec-api/login`;

export const profileApi = `${API_PREFIX_URL}/profile/`;
export const fetchServerTimeApi = `${API_PREFIX_URL}/date-time/`;
export const submissionApi = `${API_PREFIX_URL}/submissions/`;
export const editSubmissionApi = (id) => `${API_PREFIX_URL}/submissions/${id}/`;

