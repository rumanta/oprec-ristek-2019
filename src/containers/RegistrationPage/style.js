import styled from 'styled-components';
import ArrowIcon from 'assets/arrow.svg';

export const RegistrationPageContainer = styled.div`

  font-family: 'Open Sans', sans-serif;
  padding: 4rem 4rem 1rem 4rem;


  h1 {
    margin: 0;
  }

  h2 {
    margin: 1rem 0 0 0;
  }

  .titleBox {
    background: #222222;
    color: #FAFAFA;
    width: 38rem;
    padding: 0.5rem;
    font-size: 1.5rem;
  }

  .titleBox h1 {
    font-family: 'Proxima Nova', sans-serif;
    font-weight: 1000;
  }

  .heading h2 {
    font-weight: 100;
  }

  .formContainer {
    width: 100%;
    padding-top: 4rem;

  }

  .general {
    display: flex;
    flex-direction: column;
    width: 100%;
    position: relative;
    margin-bottom: 2rem;
  }

  .inputWrap {
    position: relative;
    width: 43rem;
    margin-bottom: 2.5rem;
  }
  .inputWrap .inputText{
    width: 100%;
    outline: none;
    border: none;
    border-bottom: 1px solid rgb(34, 34, 34, 0.5);
    padding-left: 0.6rem;
    padding-bottom: 0.5rem;
  }
  .inputWrap .inputText:invalid {
    box-shadow: none !important;
  }
  .inputWrap .inputText:focus{
    border-color: blue;
    border-width: medium medium 1px;
  }
  .inputWrap .floating-label {
    position: absolute;
    pointer-events: none;
    top: 18px;
    left: 10px;
    font-size: 14px;
    opacity: 0.25;
    font-family: 'Open Sans', sans-serif;
    letter-spacing: 1px;
    transition: 0.2s ease all;
  }
  .inputWrap input:focus ~ .floating-label,
  .inputWrap input:not(:focus):valid ~ .floating-label{
    top: -5px;
    left: 10px;
    font-size: 13px;
    letter-spacing: 1px;
    opacity: 0.5;
  }

  .choiceSig {
    padding-bottom: 1.5rem;
  }

  .formTitle {
    color: #589F97;
  }
  .formTitle h2 {
    font-family: 'Proxima Nova', sans-serif;
    font-weight: 1000;
    font-size: 2rem;
  }

  .selectContainer {
    margin: 4rem 0 3rem 0;
  }

  .customSelect select {
    padding: 0 0 0.5rem 0.5rem;
    border: none;
    background: none;
    outline: none;
    width: 43rem;
    border-bottom: 1px solid rgb(34, 34, 34, 0.5);
  }

  .placeHolderSel {
    font-size: 14px;
    opacity: 0.25;
  }

  .submitBtn {
    background: #589F97;
    color: #FAFAFA;
    width: 9rem;
    margin-top: 5.5rem;

    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }
  
  .submitBtn p {
    margin: 0.5rem 0 0.5rem 0;
    font-weight: 700;
  }

  h3 {
    width: 100%;
    color: #DC5539;
    margin-top: 0rem;
    margin-bottom: 1rem;
    padding-left: 0.6rem;
    line-height: 1;
    font-size: 13px;
    letter-spacing: 1px;
  }

  @media(max-width:768px) {
    padding: 4rem 2rem 1rem 2rem;

    .titleBox {
      width: 31rem;
      font-size: 1.2rem;
    }

    .inputWrap, .customSelect select {
      width: 36rem;
    }
  }

  @media(max-width:600px) {
    padding: 3rem 1rem 1rem 1rem;
    letter-spacing: 1px;

    .formContainer {
      padding-top: 2rem;
    }
  
    .titleBox {
      width: 25rem;
      font-size: 1rem;
    }

    .anggotaRistek {
      font-size: 18px;
      letter-spacing: 0px;
    }

    .inputWrap, .customSelect select {
      width: 100%;
    }

    .formTitle h2 {
      font-size: 1.5rem;
    }
  }

  @media(max-width:430px) {
    .titleBox {
      width: 22rem;
      font-size: 14px;
    }

    .anggotaRistek {
      font-size: 16px;
      letter-spacing: 0px;
    }
  }

  @media(max-width:380px) {
    .titleBox {
      letter-spacing: 0;
      width: 18rem;
      font-size: 12px;
    }

    .anggotaRistek {
      font-size: 16px;
      letter-spacing: 0px;
    }

    .general {
      margin-bottom: 1rem;
    }

    .choiceSig {
      padding-bottom: 12px;
    }

    .submitBtn {
      margin-top: 3.8rem;
    }
  }

`;
