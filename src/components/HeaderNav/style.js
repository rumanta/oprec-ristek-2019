
import styled from 'styled-components';

export const HeaderContainer = styled.div`

padding-bottom: 4rem;

  font-family: 'Proxima Nova', sans-serif;

  img {
    width: 16rem;
  }

  .headerBase {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    background:rgba(255, 255, 255, 1);
    width: 100%;

    padding: 12px 4rem 12px 4rem;
    position: fixed;
    overflow: hidden;
    height: auto;
    z-index: 999
  }

  .headerBase img {
    cursor: pointer;
  }

  .containerRight {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .rightHeader {
    display: flex;
    flex-direction: row;
    font-weight: 700;
    height: 2rem;
  }

  .leftButton {
    display: flex;
    color: #589F97;
    text-align: center;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-right: 3rem;
    cursor: pointer;
  }

  .rightButton {
    background-color: #589F97;
    color: #FAFAFA;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 1.1rem 3rem 1.1rem 3rem;
    cursor: pointer;
  }

  .rightMenuBurger {
    display: none;
    width: 2rem;
  }

  .expand {
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    z-index: 2;
    display: none;
    flex-direction: column;
    background-color: #589F97;
    padding: 1rem;
  }

  .hide {
    display: none;
  }

  .upperBurger {
    display: flex;
    justify-content: space-between;
  }

  .burgerMenus {
    width: 100%;
    height: 50%;
    display: flex;
    flex-direction: column;
    color: white;
  }

  .closeBurger {
    width: 1.5rem;
  }

  .burgerTitle {
    margin: 3rem 0 3rem 0;
    color: white;
  }

  .burgerMenus div {
    border-bottom: 0.5px solid white;
    padding-left: 1rem;
    height: 3rem;
  }
  .navContainer {
    display: flex;
    flex-direction: column;
    background: #FAFAFA;
    border-style: solid;
    border-color: #589F97;
    border-width: 1px;

    position: fixed;
    margin-top: 4.1rem;
    margin-left: 8.05rem;
    animation: fadeIn 0.25s ease-in both;
  }

  @keyframes fadeIn {
    from {
      opacity: 0;
      transform: translate3d(0, -15%, 0);
    }
    to {
      opacity: 1;
      transform: translate3d(0, 0, 0);
    }
  }

  .navBox1, .navBox2 {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0 3rem 0 3rem;
    cursor: pointer;
  }

  .navBox1 p, .navBox2 p {
    font-size: 14px;
    color: #589F97;
  }

  .navBox1 {
    border-bottom: 1px #589F97 solid;
  }

  @media(max-width:1024px) {
    .headerBase {
      padding: 12px 3rem 12px 3rem;
    }
  }

  @media(max-width:768px) {
    .headerBase {
      padding: 12px 2rem 12px 2rem;
    }
  }

  @media(max-width:728px){

    .headerBase {
      position: relative;
      padding: 0 4rem 12px 4rem;
      z-index: 0;
    }

    .menu {
      margin-bottom: 2rem;
    }

    .rightMenuBurger {
      display: block;
    }

    & {
      padding: 0;
    }

    .headerBase {
      padding: 0 1rem 0 1rem;
    }

    .ristekLogo {
      width: 12rem;
    }

    .ristekLogoBurger {
      width: 10rem;
    }

    .burgerTitle h1 {
      font-weight: 1000;
    }

    .containerRight {
      display: none;
    }

    .expand {
      display: flex;
    }
  }



`
