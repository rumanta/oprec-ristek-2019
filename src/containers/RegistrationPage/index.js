/* eslint-disable sort-keys, complexity, dot-notation, prefer-const, radix, react/jsx-key, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { RegistrationPageContainer } from './style';

import { submitForm, editForm } from './actions';
import { getUserProfile, getUser, isRegistered, isLoggedIn } from 'selectors';
import { fetchUserProfile } from 'globalActions';


class RegistrationPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nomor: '',
      idLine: '',
      email: '',
      firstChoice: 'placeholder',
      descFirst: '',
      secondChoice: 'placeholder',
      descSecond: '',
      errMessage: {
        nomor: '',
        idLine: '',
        email: '',
        firstChoice: '',
        descFirst: '',
        secondChoice: '',
        descSecond: '',
      }
    }

    this.division = {
      'Mobile Development SIG': 1,
      'Web Development SIG': 2,
      'Digital Product Design SIG': 3,
      'Game Development SIG': 4,
      'Data Science SIG': 5,
      'Competitive Programming SIG': 6,
      'Embedded System SIG': 7,
      'NetSOS SIG': 8,
      'Project Management Division': 9,
      'Public Relations Division': 10,
      'Human Resources Division': 11,
    }
  }

  componentDidMount() {
    this.props.fetchUserProfile()
    if (!this.props.isLoggedIn) {
      this.props.push('/');
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.userProfile) {
      const prof = nextProps.userProfile;

      this.setState({
        email: prof.email,
        nomor: prof.phone_number,
        idLine: prof.line,
        firstChoice: prof.first_division.name,
        descFirst: prof.first_division_reason,
        secondChoice: prof.second_division.name,
        descSecond: prof.second_division_reason,
      })
    }
  }

  formChangeInput = (field, value) => {
    const currentState = this.state;
    let newState = currentState;

    newState[field] = value;

    this.setState({
      ...newState,
    });
  }

  submitForm = () => {
    const { nomor, idLine, email, firstChoice, descFirst, secondChoice, descSecond } = this.state;

    const phoneTest = /^\d{8,12}$/;
    const emailTest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const nomorCheck = phoneTest.test(nomor);
    const idLineCheck = idLine === '' || idLine === null;
    const emailCheck = emailTest.test(email);
    const firstChoiceCheck = firstChoice === 'placeholder' || firstChoice === '' || firstChoice === null;
    const descFirstCheck = descFirst === '' || descFirst === null;
    const secondChoiceCheck = secondChoice === 'placeholder' || secondChoice === '' || secondChoice === null;
    const descSecondCheck = descSecond === '' || descSecond === null;

    if (nomorCheck && !idLineCheck && emailCheck && !firstChoiceCheck && !descFirstCheck && !secondChoiceCheck && !descSecondCheck) {

      const payload = {
        email,
        phone_number: String(nomor),
        line: idLine,
        first_division: this.division[firstChoice],
        first_division_reason: descFirst,
        second_division: this.division[secondChoice],
        second_division_reason: descSecond,
      }

      if (isRegistered) {
        this.props.editForm(payload)
      } else {
        this.props.submitForm(payload)
      }

      this.setState({
        errMessage: {
          nomor: '',
          idLine: '',
          email: '',
          firstChoice: '',
          descFirst: '',
          secondChoice: '',
          descSecond: '',
        }
      })
    } else {
      const currentState = this.state;
      let newState = currentState;

      if (!nomorCheck) {
        newState['errMessage']['nomor'] = 'Nomor telepon harus berupa angka dengan panjang minimal 8 digit';
      } else {
        newState['errMessage']['nomor'] = '';
      }

      if (idLineCheck) {
        newState['errMessage']['idLine'] = 'ID Line tidak boleh kosong';
      } else {
        newState['errMessage']['idLine'] = '';
      }

      if (!emailCheck) {
        newState['errMessage']['email'] = 'Silahkan masukkan email dengan benar';
      } else {
        newState['errMessage']['email'] = '';
      }

      if (firstChoiceCheck) {
        newState['errMessage']['firstChoice'] = 'Pilihan pertama tidak boleh kosong';
      } else {
        newState['errMessage']['firstChoice'] = '';
      }

      // if (secondChoiceCheck) {
      //   newState['errMessage']['secondChoice'] = 'Pilihan kedua tidak boleh kosong';
      // } else {
      //   newState['errMessage']['secondChoice'] = '';
      // }

      if (descFirstCheck) {
        newState['errMessage']['descFirst'] = 'Alasan tidak boleh kosong';
      } else {
        newState['errMessage']['descFirst'] = '';
      }
      if (secondChoiceCheck && !descSecondCheck) {
        newState['errMessage']['descSecond'] = 'Alasan tidak perlu diisi jika tidak memilih divisi';
      } else if (!secondChoiceCheck && descSecondCheck) {
        newState['errMessage']['descSecond'] = 'Alasan tidak boleh kosong';
      } else {
        newState['errMessage']['descSecond'] = '';
      }

      this.setState({
        ...newState,
      });
    }
  }

  render() {
    const { nomor, idLine, email, firstChoice, descFirst, secondChoice, descSecond } = this.state;

    return (
    <RegistrationPageContainer>
      <div className="container">
        <div className="heading">
        <div className="titleBox">
          <h1>
            FORM PENDAFTARAN
          </h1>
        </div>
          <h2 className="anggotaRistek">
            ANGGOTA RISTEK 2019
          </h2>
        </div>
        <div className="formContainer">
          <div className="general">
            <h3 style={{ display: this.state.errMessage.nomor === '' ? 'none' : 'block'}}>{this.state.errMessage.nomor}</h3>
            <div className="inputWrap">
              <br/>
              <input
                type="text"
                className="inputText"
                value={nomor}
                onChange={(evt) => this.formChangeInput('nomor', evt.target.value)}
                required
              />
              <span className="floating-label">Nomor Handphone</span>
            </div>

            <h3 style={{ display: this.state.errMessage.idLine === '' ? 'none' : 'block'}}>{this.state.errMessage.idLine}</h3>
            <div className="inputWrap">
              <br/>
              <input
                type="text"
                className="inputText"
                value={idLine}
                onChange={(evt) => this.formChangeInput('idLine', evt.target.value)}
                required
              />
              <span className="floating-label">ID Line</span>
            </div>

            <h3 style={{ display: this.state.errMessage.email === '' ? 'none' : 'block'}}>{this.state.errMessage.email}</h3>
            <div className="inputWrap">
              <br/>
              <input
                type="text"
                className="inputText"
                value={email}
                onChange={(evt) => this.formChangeInput('email', evt.target.value)}
                required
              />
              <span className="floating-label">Email</span>
            </div>
          </div>
          <div className="choiceSig">
            <div className="formTitle">
              <h2>
                PILIHAN PERTAMA
              </h2>
            </div>
            <div>
            <div className="selectContainer">
                <h3 style={{ display: this.state.errMessage.firstChoice === '' ? 'none' : 'block'}}>{this.state.errMessage.firstChoice}</h3>
                <div className="customSelect">
                  {
                    this.props.isRegistered ?
                    <select disabled value={firstChoice} onChange={(evt) => this.formChangeInput('firstChoice', evt.target.value)}>
                      <option disabled selected value="placeholder" className="placeholderSel">Pilih divisi untuk pilihan pertama</option>
                      <option value="Project Management Division">Project Management Division</option>
                      <option value="Public Relations Division">Public Relations Division</option>
                      <option value="Human Resources Division">Human Resources Division</option>
                      <option value="Mobile Development SIG">Mobile Development SIG</option>
                      <option value="Web Development SIG">Web Development SIG</option>
                      <option value="Digital Product Design SIG">Digital Product Design SIG</option>
                      <option value="Game Development SIG">Game Development SIG</option>
                      <option value="Data Science SIG">Data Science SIG</option>
                      <option value="Competitive Programming SIG">Competitive Programming SIG</option>
                      <option value="Embedded System SIG">Embedded System SIG</option>
                      <option value="NetSOS SIG">NetSOS SIG</option>
                    </select>
                    :
                    <select value={firstChoice} onChange={(evt) => this.formChangeInput('firstChoice', evt.target.value)}>
                      <option disabled selected value="placeholder" className="placeholderSel">Pilih divisi untuk pilihan pertama</option>
                      <option value="Project Management Division">Project Management Division</option>
                      <option value="Public Relations Division">Public Relations Division</option>
                      <option value="Human Resources Division">Human Resources Division</option>
                      <option value="Mobile Development SIG">Mobile Development SIG</option>
                      <option value="Web Development SIG">Web Development SIG</option>
                      <option value="Digital Product Design SIG">Digital Product Design SIG</option>
                      <option value="Game Development SIG">Game Development SIG</option>
                      <option value="Data Science SIG">Data Science SIG</option>
                      <option value="Competitive Programming SIG">Competitive Programming SIG</option>
                      <option value="Embedded System SIG">Embedded System SIG</option>
                      <option value="NetSOS SIG">NetSOS SIG</option>
                    </select>
                  }
                </div>
              </div>

                <h3 style={{ display: this.state.errMessage.descFirst === '' ? 'none' : 'block'}}>{this.state.errMessage.descFirst}</h3>
                <div className="inputWrap">
                <br/>
                <input
                  type="text"
                  className="inputText"
                  value={descFirst}
                  onChange={(evt) => this.formChangeInput('descFirst', evt.target.value)}
                  required
                />
                <span className="floating-label">Alasan memilih divisi tersebut</span>
              </div>
            </div>
          </div>

          <div className="choiceSig" style={{marginBottom: '8rem'}}>
            <div className="formTitle">
              <h2>
                PILIHAN KEDUA
              </h2>
            </div>
            <div>
              <div className="selectContainer">
                <h3 style={{ display: this.state.errMessage.secondChoice === '' ? 'none' : 'block'}}>{this.state.errMessage.secondChoice}</h3>
                <div className="customSelect">
                {
                  this.props.isRegistered ?
                    <select disabled value={secondChoice} onChange={(evt) => this.formChangeInput('secondChoice', evt.target.value)}>
                      <option disabled selected value="placeholder" className="placeholderSel">Pilih divisi untuk pilihan pertama</option>
                      <option value="Project Management Division">Project Management Division</option>
                      <option value="Public Relations Division">Public Relations Division</option>
                      <option value="Human Resources Division">Human Resources Division</option>
                      <option value="Mobile Development SIG">Mobile Development SIG</option>
                      <option value="Web Development SIG">Web Development SIG</option>
                      <option value="Digital Product Design SIG">Digital Product Design SIG</option>
                      <option value="Game Development SIG">Game Development SIG</option>
                      <option value="Data Science SIG">Data Science SIG</option>
                      <option value="Competitive Programming SIG">Competitive Programming SIG</option>
                      <option value="Embedded System SIG">Embedded System SIG</option>
                      <option value="NetSOS SIG">NetSOS SIG</option>
                    </select>
                    :
                    <select value={secondChoice} onChange={(evt) => this.formChangeInput('secondChoice', evt.target.value)}>
                      <option disabled selected value="placeholder" className="placeholderSel">Pilih divisi untuk pilihan pertama</option>
                      <option value="Project Management Division">Project Management Division</option>
                      <option value="Public Relations Division">Public Relations Division</option>
                      <option value="Human Resources Division">Human Resources Division</option>
                      <option value="Mobile Development SIG">Mobile Development SIG</option>
                      <option value="Web Development SIG">Web Development SIG</option>
                      <option value="Digital Product Design SIG">Digital Product Design SIG</option>
                      <option value="Game Development SIG">Game Development SIG</option>
                      <option value="Data Science SIG">Data Science SIG</option>
                      <option value="Competitive Programming SIG">Competitive Programming SIG</option>
                      <option value="Embedded System SIG">Embedded System SIG</option>
                      <option value="NetSOS SIG">NetSOS SIG</option>
                    </select>
                  }
                </div>
              </div>
                <h3 style={{ display: this.state.errMessage.descSecond === '' ? 'none' : 'block'}}>{this.state.errMessage.descSecond}</h3>
                <div className="inputWrap">
                <br/>
                <input
                  type="text"
                  className="inputText"
                  value={descSecond}
                  onChange={(evt) => this.formChangeInput('descSecond', evt.target.value)}
                  required
                />
                <span className="floating-label">Alasan memilih divisi tersebut</span>
              </div>
            </div>
            <div className="submitBtn" onClick={() => this.submitForm()}>
              <p>SUBMIT</p>
            </div>
          </div>
        </div>
      </div>
    </RegistrationPageContainer>
    );
  }
}

RegistrationPage.propTypes = {
  push: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  editForm: PropTypes.func.isRequired,
  userProfile: PropTypes.shape(),
  isRegistered: PropTypes.bool.isRequired,
  fetchUserProfile: PropTypes.func.isRequired,
}

function mapStateToProps(state) {
  return {
    userProfile: getUserProfile(state),
    user: getUser(state),
    isRegistered: isRegistered(state),
    isLoggedIn: isLoggedIn(state),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    submitForm: (data) => dispatch(submitForm(data)),
    editForm: (data) => dispatch(editForm(data)),
    fetchUserProfile: () => dispatch(fetchUserProfile()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationPage);
