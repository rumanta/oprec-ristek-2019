/* eslint-disable */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { AppContainer } from './style';

import { theme } from './theme';
import { routes } from './routes';
import Header from 'components/HeaderNav';
import Footer from 'components/Footer';

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet"></link>

export default class App extends React.Component {
  render() {
    const pages = routes.map(route => (
      <Route
        component={route.component}
        exact={route.exact}
        path={route.path}
      />
    ));

    return (
      <ThemeProvider theme={theme}>
        <AppContainer>
          <Header />
          <Switch>
              {pages}
          </Switch>
          <Footer />
        </AppContainer>
      </ThemeProvider>
    );
  }
}
