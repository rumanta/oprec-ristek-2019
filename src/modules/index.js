/* eslint-disable */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import globalReducer from 'globalReducer';
import registrationReducer from 'containers/RegistrationPage/reducer';

export default combineReducers({
  // 'routing': routerReducer,
  // 'pickSchedulePageReducer': pickSchedulePageReducer,
  // 'navReducer': navReducer,
  // 'showSchedulePageReducer': showSchedulePageReducer,
  // 'formReducer': registrationReducer,
  'global': globalReducer,
});
