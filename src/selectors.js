import { getCookie } from 'utils';

export const isLoggedIn = (state) => state.global.get('loggedIn');
export const getUser = (state) => state.global.get('user').toJS();
export const getServerTime = (state) => state.global.get('serverTime');
export const isRegistered = (state) => Boolean(getUser(state).profile);
export const getUserProfile = (state) => getUser(state).profile;
export const getRegisteredForm = (state) => state.formReducer.get('registeredForm');

