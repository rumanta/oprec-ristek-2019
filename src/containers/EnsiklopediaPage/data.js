export const DIVISI = [
  {
    title: 'Human Resource',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Public Relation',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Project Management',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Digital Product Development SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Game Development SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Mobile Development SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Web Development SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Competitive Programming SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Data Science SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Embedded System SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
  {
    title: 'Network Security & Operating System SIG',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget est interdum, varius nibh ac, vehicula lacus. Morbi imperdiet ac velit quis facilisis. Vestibulum ac purus quis mauris rhoncus ultrices vel et lectus.',
  },
];

export const PROSEDUR = [
  {
    title: 'Prosedur Open Recruitment',
    content: 'Malesuada hendrerit a litora ultrices vitae libero in enim scelerisque scelerisque sagittis facilisi adipiscing ipsum dui purus orci dictumst nullam quis laoreet magna a vel. A ullamcorper a lorem diam orci vestibulum id nisi quis eros ullamcorper elementum fermentum a non tincidunt scelerisque dui vel a dui vestibulum. Augue vestibulum nascetur pharetra inceptos libero non in turpis ad condimentum elit condimentum pulvinar adipiscing vestibulum velit tortor a suspendisse senectus a nec neque eu turpis habitasse ante. Sapien curae pulvinar id magna egestas fringilla penatibus parturient condimentum natoque in mauris fusce parturient auctor nam quam parturient a diam urna dolor metus lacinia vestibulum dui sociosqu nullam. Consequat vestibulum vestibulum maecenas eros primis lectus ante molestie dapibus himenaeos viverra class natoque natoque felis dui potenti a vestibulum a aliquet nisl magnis eu a. Euismod at ligula fringilla at a cum facilisi vestibulum blandit duis ligula consectetur nibh dictumst scelerisque parturient diam integer a ante a adipiscing rhoncus ante curabitur eu orci. Taciti integer suspendisse scelerisque scelerisque euismod pharetra mus accumsan justo et vestibulum a parturient hac eu enim.<br />Consequat ad nascetur tristique sodales erat parturient sem praesent a ligula tristique adipiscing a ornare.Parturient justo gravida dictumst eu adipiscing a eu vestibulum.',
  },
  {
    title: 'Persyaratan Anggota Ristek Fasilkom UI 2019',
    content: 'Malesuada hendrerit a litora ultrices vitae libero in enim scelerisque scelerisque sagittis facilisi adipiscing ipsum dui purus orci dictumst nullam quis laoreet magna a vel. A ullamcorper a lorem diam orci vestibulum id nisi quis eros ullamcorper elementum fermentum a non tincidunt scelerisque dui vel a dui vestibulum. Augue vestibulum nascetur pharetra inceptos libero non in turpis ad condimentum elit condimentum pulvinar adipiscing vestibulum velit tortor a suspendisse senectus a nec neque eu turpis habitasse ante. Sapien curae pulvinar id magna egestas fringilla penatibus parturient condimentum natoque in mauris fusce parturient auctor nam quam parturient a diam urna dolor metus lacinia vestibulum dui sociosqu nullam. Consequat vestibulum vestibulum maecenas eros primis lectus ante molestie dapibus himenaeos viverra class natoque natoque felis dui potenti a vestibulum a aliquet nisl magnis eu a. Euismod at ligula fringilla at a cum facilisi vestibulum blandit duis ligula consectetur nibh dictumst scelerisque parturient diam integer a ante a adipiscing rhoncus ante curabitur eu orci. Taciti integer suspendisse scelerisque scelerisque euismod pharetra mus accumsan justo et vestibulum a parturient hac eu enim.<br />Consequat ad nascetur tristique sodales erat parturient sem praesent a ligula tristique adipiscing a ornare.Parturient justo gravida dictumst eu adipiscing a eu vestibulum.',
  },
];
