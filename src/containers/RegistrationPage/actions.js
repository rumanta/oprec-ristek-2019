import axios from 'axios';
import swal from 'sweetalert';
import { push } from 'react-router-redux';
import auth from 'auth';

import {
    SUBMIT_REGISTRATION,
    SUBMIT_REGISTRATION_SUCCESS,
    SUBMIT_REGISTRATION_FAILED,
    FETCH_REGISTRATION,
    FETCH_REGISTRATION_SUCCESS,
    FETCH_REGISTRATION_FAILED
} from './constants';

import { profileApi } from 'api';

export function submitForm(data) {

  return (dispatch) => {
    dispatch({
        type: SUBMIT_REGISTRATION
    })

    axios.post(profileApi, data)
      .then((response) => {
        swal("Submit Success!", "Registrasi telah berhasil", "success");
        dispatch({
          payload: response.data,
          type: SUBMIT_REGISTRATION_SUCCESS,
        })
        this.props.push('/home')
      })
      .catch((err) => {
        swal("Submit Failed!", "Registrasi gagal", "error");
        dispatch({
          payload: err.response,
          type: SUBMIT_REGISTRATION_FAILED
        })
      })
  }
}

export function editForm(data) {

  return (dispatch) => {
    dispatch({
        type: SUBMIT_REGISTRATION
    })

    axios.patch(profileApi, data)
      .then((response) => {
        swal("Submit Success!", "Edit form registrasi telah berhasil", "success");
        dispatch({
          payload: response.data,
          type: SUBMIT_REGISTRATION_SUCCESS,
        })
      })
      .catch((err) => {
        swal("Submit Failed!", "Edit form registrasi gagal", "error");
        dispatch({
          payload: err.response,
          type: SUBMIT_REGISTRATION_FAILED
        })
      })
  }
}