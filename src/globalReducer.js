/*
 *
 * Global reducer
 *
 */

 /* eslint-disable sort-keys, no-case-declarations, radix, react/jsx-key, operator-linebreak, object-property-newline, no-lonely-if */

import { fromJS } from 'immutable';
import { isEmpty } from 'lodash';
import auth from 'auth';
import {
  SET_AUTH,
  SET_USER,
  SET_SERVER_TIME,
  SENDING_REQUEST,
  REQUEST_ERROR,
  CLEAR_ERROR
} from './globalConstants';

import {
  SUBMIT_REGISTRATION_SUCCESS,
  SUBMIT_REGISTRATION_FAILED,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_FAILED,
} from 'containers/RegistrationPage/constants';

import {
  UPLOAD_SUBMISSION_SUCCESS,
  EDIT_SUBMISSION_SUCCESS,
  EDIT_SUBMISSION_FAILED,
  UPLOAD_SUBMISSION_FAILED,
  FETCH_SUBMISSION_SUCCESS,
  FETCH_SUBMISSION_FAILED,
} from 'containers/DashboardPage/constants';

const isLoggedIn = auth.loggedIn();

const initialState = fromJS({
  currentlySending: false,
  error: '',
  loggedIn: !isEmpty(isLoggedIn),
  serverTime: '',
  user: isEmpty(isLoggedIn) ? {} : isLoggedIn,
});

function globalReducer(state = initialState, action) {
  switch (action.type) {
    case SET_AUTH:
      return state.set('loggedIn', action.newAuthState);
    case SET_USER:
      return state.set('user', fromJS(action.user));

    // eslint-disable-next-line no-case-declarations
    case SENDING_REQUEST:
      return state.set('currentlySending', action.sending);
    case REQUEST_ERROR:
      return state.set('error', action.error);
    case CLEAR_ERROR:
      return state.set('error', '');
    case SET_SERVER_TIME:
      return state.set('serverTime', action.payload);

    case UPLOAD_SUBMISSION_SUCCESS:
      const subUser = state.get('user').toJS();

      const newSubUser = {
        ...subUser,
        profile: {
          ...subUser.profile,
          submissions: action.payload,
        }
      }

      return state.set('user', fromJS(newSubUser));
    case EDIT_SUBMISSION_SUCCESS:
      const editSubUser = state.get('user').toJS();
      const listSubmission = editSubUser.profile.submissions;

      const result = [...listSubmission.filter((sub) => {
        return sub.task !== action.payload.task
      }), action.payload];

      const newEditSubUser = {
        ...editSubUser,
        profile: {
          ...editSubUser.profile,
          submissions: result,
        }
      }

      return state.set('user', fromJS(newEditSubUser));
    case EDIT_SUBMISSION_FAILED:
      return state.set('errorSub', action.payload);
    case UPLOAD_SUBMISSION_FAILED:
      return state.set('errorSub', action.payload);
    case FETCH_SUBMISSION_SUCCESS:
      return state.set('submission', action.payload);
    case FETCH_SUBMISSION_FAILED:
      return state.set('errorSub', action.payload);

    case SUBMIT_REGISTRATION_SUCCESS:

      const oldUser = state.get('user').toJS();

      const newUser = {
        ...oldUser,
        profile: action.payload
      }
      return state.set('user', fromJS(newUser));

    case SUBMIT_REGISTRATION_FAILED:
      return state.set('error', action.payload);
    case FETCH_USER_PROFILE_SUCCESS:
    const user = state.get('user').toJS();

    const newFormedUser = {
      ...user,
      profile: action.payload
    }

    return state.set('user', fromJS(newFormedUser));
    case FETCH_USER_PROFILE_FAILED:
      return state.set('error', action.payload);
    default:
      return state;
  }
}

export default globalReducer;
