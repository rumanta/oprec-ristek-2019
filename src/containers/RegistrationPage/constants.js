export const SUBMIT_REGISTRATION = 'src/containers/RegistrationPage/SUBMIT_REGISTRATION';
export const SUBMIT_REGISTRATION_SUCCESS = 'src/containers/RegistrationPage/SUBMIT_REGISTRATION_SUCCESS';
export const SUBMIT_REGISTRATION_FAILED = 'src/containers/RegistrationPage/SUBMIT_REGISTRATION_FAILED';

export const FETCH_USER_PROFILE = 'src/containers/RegistrationPage/FETCH_USER_PROFILE';
export const FETCH_USER_PROFILE_SUCCESS = 'src/containers/RegistrationPage/FETCH_USER_PROFILE_SUCCESS';
export const FETCH_USER_PROFILE_FAILED = 'src/containers/RegistrationPage/FETCH_USER_PROFILE_FAILED';
