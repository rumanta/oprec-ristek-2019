export const UPLOAD_SUBMISSION = 'src/containers/DashboardPage/UPLOAD_SUBMISSION';
export const UPLOAD_SUBMISSION_SUCCESS = 'src/containers/DashboardPage/UPLOAD_SUBMISSION_SUCCESS';
export const UPLOAD_SUBMISSION_FAILED = 'src/containers/DashboardPage/UPLOAD_SUBMISSION_FAILED';

export const FETCH_SUBMISSION = 'src/containers/DashboardPage/FETCH_SUBMISSION';
export const FETCH_SUBMISSION_SUCCESS = 'src/containers/DashboardPage/FETCH_SUBMISSION_SUCCESS';
export const FETCH_SUBMISSION_FAILED = 'src/containers/DashboardPage/FETCH_SUBMISSION_FAILED';

export const EDIT_SUBMISSION = 'src/containers/DashboardPage/EDIT_SUBMISSION';
export const EDIT_SUBMISSION_SUCCESS = 'src/containers/DashboardPage/EDIT_SUBMISSION_SUCCESS';
export const EDIT_SUBMISSION_FAILED = 'src/containers/DashboardPage/EDIT_SUBMISSION_FAILED';