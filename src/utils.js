export const setCookie = (data) => {
  const date = new Date();
  date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));
  const expires = `expires=${date.toUTCString()}`;
  document.cookie = `major_id=${data.major_id}; expires=${expires};path=/`;
  document.cookie = `token=${data.token}; expires=${expires};path=/`;
  document.cookie = `user_id=${data.user_id}; expires=${expires};path=/`;
}

export const getCookie = (cname) => {
  const name = `${cname}=`;
  const cookieArray = document.cookie.split(';');

  const res = cookieArray.find(cookie => (
    cookie.trim().indexOf(name) === 0)
  )
  if (res) {
    return res.trim().substring(cname.length + 1, res.length);
  }

  return '';
}