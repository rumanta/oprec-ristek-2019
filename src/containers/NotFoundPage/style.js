import styled from 'styled-components';

export const NotFoundPageStyle = styled.div`
  font-family: 'Proxima Nova', sans-serif;

  .fourOFourContainer {
    display: flex;
    padding: 10rem 4rem 5rem 4rem;
  }

  .leftContent {
    width: 85%;
    height: 18rem;
    margin-right: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .leftContent img {
    height: 13rem;
  }

  ${'' /* .fourOFour {
    color: white;
    margin: 0;
    text-shadow: -2px 0 black, 0 2px black, 2px 0 black, 0 -2px black;
  } */}

  .rightContent {
    display: flex;
    color: white;
    background: #222222;
    height: 18rem;
    width: 100%;
  }

  .centerContent {
    display: flex;
    flex-direction: column;
    margin: 5rem 0 5rem 2rem;
    justify-content: center;
  }

  .centerContent h1,p {
    margin: 0;
  }

  .lost {
    font-size: 56px;
  }

  .goHome {
    font-size: 25px;
  }

  @media(max-width:728px){

      .fourOFourContainer {
        flex-direction: column;
        height: 100%;
        padding: 10rem 2rem 5rem 2rem;
      }

      .leftContent, .rightContent {
        width: 100%;
        height: 10rem;
        font-size: 75px;
        margin-right: 0;
        margin-bottom: 2rem;
      }

      .leftContent img {
        padding-bottom: 3rem;
      }

      .lost {
        font-size: 45px;
      }

      .goHome {
        font-size: 20px;
      }
  }

  @media(max-width: 480px) {
    .leftContent img {
      height: 10rem;
    }
  }

`
