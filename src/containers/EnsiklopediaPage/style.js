import styled from 'styled-components';

export const EnsiklopediaPageContainer = styled.div`
  font-family: 'Open Sans', sans-serif;
  margin-bottom: 4rem;

  .container {
    padding: 4rem 4rem 4rem 4rem;
  }

  .upperTitle {
    display: flex;
    color: white;
    background: #222222;
    width: 19rem;
    padding: 0.5rem 0.5rem 0.5rem;
  }

  h1 {
    margin: 0;
  }

  .pageTitle {
    font-weight: 1000;
  }

  .content {
    display: flex;
    flex-direction: row;
    width: 100%;
    margin-top: 1rem;
  }

  .leftContent {
    font-family: 'Open Sans', sans-serif;
    width: 95%;
    margin-right: 2rem;
  }

  .rightContent {
    width: 100%;
  }

  .contentHeader {
    color: #589F97;
  }

  @media(max-width:728px){

    .container {
        padding: 3rem 1rem 4rem 1rem;
      }
      .content {
        flex-direction: column;
      }

      .leftContent {
        width: 100%;
        margin-right: 0;
        margin-bottom: 4rem;
      }

  }

`;
