/* eslint-disable sort-keys, radix, react/jsx-key, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { FooterContainer } from './style';

import FooterIcon from 'assets/footerset.svg';
import TextIcon from 'assets/text.svg';


export default class Footer extends React.Component {

  render() {

    return (
    <FooterContainer>
      <div className="footerContainer">
        <div className="leftContainer">
          <div className="leftFooter">
            <img className="conquer" src={TextIcon} />
          </div>
        </div>
        <div className="rightFooter">
          <div className="linkRistek">
            <p>
              ristek.cs.ui.ac.id
            </p>
            <p>
              fb.me/RistekCSUI
            </p>
          </div>
          <div>
            <img className="footerIcon" src={FooterIcon} />
          </div>
        </div>
      </div>
    </FooterContainer>
    );
  }
}
