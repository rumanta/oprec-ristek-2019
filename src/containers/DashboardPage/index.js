/* eslint-disable sort-keys, radix, react/jsx-key, operator-linebreak, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import isEmpty from 'lodash';
import { connect } from 'react-redux';
import { DashboardPageContainer } from './style';
import UpArrowIcon from 'assets/uparrow.svg';
import ChecklistIcon from 'assets/checklist.svg';
import TimeWidget from 'components/TimeWidget';
import Files from 'react-files'
import Spinner from 'components/Spinner';

import { uploadSubmission, editSubmission } from './actions';
import { isLoggedIn, getUser, getServerTime, isRegistered, getUserProfile } from 'selectors';
import { fetchServerTime, fetchUserProfile } from 'globalActions';

const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli',
'Agustus', 'September', 'Oktober', 'November', 'Desember'];

class DashboardPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      profile: {},
      files: [],
    }
  }

  componentDidMount() {
    if (!this.props.isLoggedIn) {
      this.props.push('/');
    }
    this.props.fetchServerTime();
    this.props.fetchUserProfile();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      const name = this.trimUsername(nextProps.user.name);
      const profile = nextProps.user.profile;

      this.setState({
        name,
        profile,
      })
    }
  }

  parseServerTime = () => {
    if (this.props.serverTime) {
      const { serverTime } = this.props;
      const datetime = serverTime.split(' ');
      const [date, time] = [datetime[0].split('-'), datetime[1].split(':')];

      const timeObj = new Date(
        parseInt(date[0]),
        parseInt(date[1]) - 1,
        parseInt(date[2]),
        parseInt(time[0]),
        parseInt(time[1]),
        parseInt(time[2]),
        0,
      )

      return timeObj;
    }

    return null;
  }

  trimUsername = (name) => (
    name && name.split(' ')[0].toUpperCase()
  );

  onFilesChangeSubmitted = (subId) => (files) => {
    const data = new FormData();
    data.append('attachment', files[0])
    this.props.editSubmission(data, subId)
  }

  onFilesChange = (taskId) => (files) => {
    const data = new FormData();
    data.append('task', taskId)
    data.append('attachment', files[0])
    this.props.uploadSubmission(data)
  }

  onFilesError = (taskId) => (error) => {
    // console.log('error code ' + error.code + ': ' + error.message)
  }

  renderRegisterNow = () => (
    <div className="registerCont">
      <div>
        <p className="notice">Kamu belum pernah mendaftar apapun</p>
        <h2 className="registerNow" onClick={() => this.props.push('/form')}>
          <span>DAFTAR SEKARANG JUGA</span>
        </h2>
      </div>
    </div>
  );

  renderSubmission = (isSubmitted, taskId) => {
    if (isSubmitted) {
      const { profile } = this.state;
      let currentSub;
      if (profile.submissions) {
        profile.submissions.map((sub) => {
          if (sub.task === taskId) {
            currentSub = sub;
          }
        })
      }

      const downloadLink = currentSub.attachment;
      const dt = new Date(currentSub.updated_at);

      const lastUpdated = `${dt.getDate()} ${months[dt.getMonth()]} ${dt.getFullYear()}, ${dt.getHours()}:${dt.getMinutes()}`

      return (
        <Files
          ref="files"
          className="submissionBox"
          onChange={this.onFilesChangeSubmitted(currentSub.id)}
          onError={this.onFilesError(currentSub.id)}
          multiple={false}
          maxFiles={10000}
          maxFileSize={6000000}
          minFileSize={0}
        >
          <div className="submissionIcon">
            <img src={ChecklistIcon} />
          </div>
          <div className="subContent">
            <p className="lastEdit">LAST EDIT : {lastUpdated}</p>
            <div className="editDownload">
              <p className="clickDownload">click to download submission file, or</p>
              <p className="specialEdit"><span>Edit</span></p>
            </div>
            <div className="editSubBtn">
              <p className="subText">
                EDIT SUBMISSION
              </p>
            </div>
          </div>
          </Files>
      );
    }

    return (
      <Files
        ref="files"
        className="submissionBox"
        style={{background: '#F0F0F0'}}
        onChange={this.onFilesChange(taskId)}
        onError={this.onFilesError(taskId)}
        multiple={false}
        maxFiles={1}
        maxFileSize={6000000}
        minFileSize={0}
      >
        <div className="submissionIcon2">
          <img src={UpArrowIcon} />
        </div>
        <div className="subContent">
          <p className="dragDrop">DRAG AND DROP</p>
          <div className="editDownload">
            <p className="clickUpload">your file to upload, or</p>
            <p className="specialBrowse">
              <span>browse</span>
            </p>
          </div>
        </div>
      </Files>
    );
  }

  renderAssignment = () => {
    const { user } = this.props;
    const profile = user.profile
    let firstChoiceSubmitted = false;
    let secondChoiceSubmitted = false;

    let taskIdSecond = 9999;

    const taskIdFirst = profile.first_division.tasks[0].id;
    if (profile.second_division) {
      taskIdSecond = profile.second_division.tasks[0].id;
    }

    let taskGeneralLink = profile.tasks.find((task) => !task.division); 
    let taskFirstLink = profile.tasks.find((task) => task.id === taskIdFirst);
    let taskSecondLink = profile.tasks.find((task) => task.id === taskIdSecond);
    taskFirstLink = taskFirstLink && taskFirstLink.attachment;
    taskSecondLink = taskSecondLink && taskSecondLink.attachment;
    taskGeneralLink = taskGeneralLink && taskGeneralLink.attachment;

    if (profile.submissions) {
      profile.submissions.map((sub) => {
        if (sub.task === taskIdFirst) {
          firstChoiceSubmitted = true
        } else if (sub.task === taskIdSecond) {
          secondChoiceSubmitted = true
        }
      })
    }

    return (
      <div>
        <div className="generalTask">
          <div className="titleCont">
            <h1 className="taskTitle">
              TUGAS
            </h1>
            <h1 className="taskTitle">
              GENERAL
            </h1>
          </div>
          <p className="descChoiceTask">Tugas untuk seluruh calon anggota Ristek 2019</p>
          <p onClick={() => window.open(taskGeneralLink, '_blank', 'toolbar=0,location=0,menubar=0')} className="download"><span>DOWNLOAD TUGAS</span></p>
        </div>
        <div className="vectorLine"></div>
        <div className="submissionCont">
          <div className="choiceTask">
            <div className="titleCont">
              <h1 className="taskTitle">
                TUGAS
              </h1>
              <h1 className="taskTitle">
                PILIHAN 1
              </h1>
            </div>
            <p className="descChoiceTask">{profile.first_division.name}</p>
            <p onClick={() => window.open(taskFirstLink, '_blank', 'toolbar=0,location=0,menubar=0')} className="download"><span>DOWNLOAD TUGAS</span></p>
          </div>
          <div className="subBoxCont">
              {this.renderSubmission(firstChoiceSubmitted, taskIdFirst)}
          </div>
        </div>
        <div className="vectorLine"></div>
        { !profile.second_division ? (
          <React.Fragment>
            <div className="submissionCont">
              <div className="choiceTask">
                <div className="titleCont">
                  <h1 className="taskTitle">
                    TUGAS
                  </h1>
                  <h1 className="taskTitle">
                    PILIHAN 2
                  </h1>
                </div>
                <p className="descChoiceTask">{profile.second_division.name}</p>
                <p onClick={() => window.open(taskSecondLink, '_blank', 'toolbar=0,location=0,menubar=0')} className="download"><span>DOWNLOAD TUGAS</span></p>
              </div>
              <div className="subBoxCont">
                {this.renderSubmission(secondChoiceSubmitted, taskIdSecond)}
              </div>
            </div>
            <div className="vectorLine"></div>
          </React.Fragment>)
            :
            <div style={{marginBottom: '4rem'}}></div>
          }
        <div className="editRegisterCont">
          <p className="descChoiceTask">Edit terakhir pada 20 Februari 2019, 17.00 PM</p>
          <h2 disabled className="registerNow" onClick={() => this.props.push('/form')}>
            <span>EDIT PENDAFTARAN</span>
          </h2>
        </div>
      </div>
      );
    }

  render() {
    const { isRegistered } = this.props;

    const time = this.parseServerTime();

    if (!this.props.serverTime && !this.props.user) {
      return (
        <DashboardPageContainer>
            <div className="midLoader">
                <Spinner />
            </div>
        </DashboardPageContainer>
      );
    }

    return (
    <DashboardPageContainer>
      <div className="container">
        <div className="heading">
          <h1 className="greeting">
              HALO, {this.state.name}!
          </h1>
          <p className="deadlineText">
              Batas open recruitment anggota ristek hanya tersisa
          </p>
        </div>
        {<TimeWidget serverTime={time} />}
        <div className="vectorLine"></div>
        {
          !isRegistered ?
          this.renderRegisterNow()
          :
          this.renderAssignment()
        }
      </div>
    </DashboardPageContainer>
    );
  }
}

DashboardPage.propTypes = {
  push: PropTypes.func.isRequired,
  fetchServerTime: PropTypes.func.isRequired,
  fetchUserProfile: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  isRegistered: PropTypes.bool.isRequired,
  serverTime: PropTypes.string.isRequired,
  user: PropTypes.shape(),
  userProfile: PropTypes.shape(),
  submission: PropTypes.shape(),
  uploadSubmission: PropTypes.func.isRequired,
}

function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
    serverTime: getServerTime(state),
    isRegistered: isRegistered(state),
    user: getUser(state),
    userProfile: getUserProfile(state)
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    fetchServerTime: () => dispatch(fetchServerTime()),
    fetchUserProfile: () => dispatch(fetchUserProfile()),
    uploadSubmission: (data) => dispatch(uploadSubmission(data)),
    editSubmission: (data, subId) => dispatch(editSubmission(data, subId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);
