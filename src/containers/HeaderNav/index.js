/* eslint-disable sort-keys, no-return-assign, radix, react/jsx-key, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { HeaderContainer } from './style';

import LogoSet from 'assets/logoset.svg';


class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      expandNav: false,
    }
  }

  UNSAFE_componentWillMount() {
    document.addEventListener('mousedown', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false);
  }

  handleClick = (evt) => {
    if (this.node) {
      if (!this.node.contains(evt.target) && !this.btn.contains(evt.target)) {
        this.setState({
          expandNav: false
        });
      }
    }
  }

  onClickNavBtn = () => {
    if (this.state.expandNav) {
      this.setState({
        expandNav: false,
      })
    } else {
      this.setState({
        expandNav: true,
      })
    }
  }

  movingPage = (route) => {
    this.props.push(route)
    this.setState({
      expandNav: false,
    })
  }

  trimUsername = (name) => (
    name.split('.')[0].toUpperCase()
  );

  renderExpansionNav = () => (
    <div ref={node => this.node = node} className="navContainer">
      <div className="navBox1" onClick={() => this.movingPage('/home')}>
        <p>DASHBOARD</p>
      </div>
      <div className="navBox2"}>
        <p>LOGOUT</p>
      </div>
    </div>
  );

  render() {
    const loggedIn = true;

    return (
    <HeaderContainer>
      <div className="headerBase">
        <img src={LogoSet} onClick={() => this.movingPage('/home')} />
        <div className="containerRight">
          <div className="rightHeader">
            <div onClick={() => this.movingPage('/ensiklopedia')} className="leftButton">ENSIKLOPEDIA</div>
            {
              loggedIn ?
              <div ref={btn => this.btn = btn} onClick={() => this.onClickNavBtn()} className="rightButton">{this.trimUsername('ivan.abdurahman')}</div>
              :
              <div className="rightButton">LOGIN SSO</div>
            }
          </div>
          {this.state.expandNav && this.renderExpansionNav()}
        </div>
      </div>
    </HeaderContainer>
    );
  }
}

Header.propTypes = {
  push: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
}

function mapStateToProps(state) {
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);