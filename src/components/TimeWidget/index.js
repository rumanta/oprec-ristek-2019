/* eslint-disable sort-keys, no-bitwise, no-plusplus, radix, react/jsx-key, operator-linebreak, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { TimeWidgetContainer } from './style';

export default class TimeWidget extends React.Component {

  constructor(props) {
    super(props);
    this.deadline = new Date(2019, 1, 21, 0, 0, 0);
    this.state = {
      seconds: 0,
      minutes: 0,
      hours: 0,
      days: 0,
      serverTime: null,
      remainSec: 0,
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.serverTime) {
      const serverTime = nextProps.serverTime.getTime();
      this.setState({
        serverTime
      })
      const diffDate = this.deadline.getTime() - serverTime;
      const remainSec = Math.abs(diffDate / 1000);

      this.setState({
        remainSec
      })
    }
  }

  tick = () => {
    let { remainSec } = this.state;
    const fixRemainSec = remainSec;

    const days = Math.floor(remainSec / 86400);
    remainSec %= 86400;
    const hours = Math.floor(remainSec / 3600);
    remainSec %= 3600;
    const minutes = Math.floor(remainSec / 60);
    remainSec %= 60;
    const seconds = Math.floor(remainSec);

    this.setState({
      days,
      hours,
      minutes,
      seconds,
      remainSec: fixRemainSec - 1,
    });

    if (days === 0 & hours === 0 & minutes === 0 & seconds === 0) {
      clearInterval(this.intervalHandle);
    }
  }

  startCountDown = () => {
    clearInterval(this.intervalHandle);
    this.intervalHandle = setInterval(this.tick, 1000);
  }

  render() {
    const {days, hours, minutes, seconds} = this.state;

    if (this.props.serverTime) {
      // () => this.initServerTime()
      this.startCountDown();
    }

    return (
      <TimeWidgetContainer>
        <div className="timeWidget">
          <div className="boxCont">
          <div className="blackBox">
            {
              days < 10 ?
              <h1 className="timeText">0{days}</h1>
              :
              <h1 className="timeText">{days}</h1>
            }
          </div>
          <p className="descText">HARI</p>
          </div>
          <div className="boxCont">
          <div className="blackBox">
            {
              hours < 10 ?
              <h1 className="timeText">0{hours}</h1>
              :
              <h1 className="timeText">{hours}</h1>
            }
          </div>
          <p className="descText">JAM</p>
          </div>
          <div className="boxCont">
          <div className="blackBox">
            {
              minutes < 10 ?
              <h1 className="timeText">0{minutes}</h1>
              :
              <h1 className="timeText">{minutes}</h1>
            }
          </div>
          <p className="descText">MENIT</p>
          </div>
          <div className="boxCont">
          <div className="blackBox">
            {
              seconds < 10 ?
              <h1 className="timeText">0{seconds}</h1>
              :
              <h1 className="timeText">{seconds}</h1>
            }
          </div>
          <p className="descText">DETIK</p>
          </div>
        </div>
      </TimeWidgetContainer>
    );
  }
}