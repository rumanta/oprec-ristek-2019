import styled from 'styled-components';

export const AppContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 100%;
  overflow: hidden;
  min-height: 100vh;
  z-index: 1;
  color: #222222;
  letter-spacing: 2px;

  @font-face {
    font-family: 'Proxima Nova';
    src: url('assets/proximanova.ttf').format('truetype');
    font-weight: normal;
    font-style: normal;
}

`;
