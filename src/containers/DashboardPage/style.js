import styled from 'styled-components';

export const DashboardPageContainer = styled.div`

  font-family: 'Proxima Nova', sans-serif;

  .container {
    padding: 4rem 4rem 1rem 4rem;
  }

  .midLoader{
    width: 100%;
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }

  .greeting {
    font-weight: 1000;
    font-size: 3rem;
    margin: 0
  }

  .vectorLine {
    margin-top: 2rem;
    margin-bottom: 2rem;
    border-bottom: 2px solid #DDDDDD;
  }

  h2.registerNow span {
    font-weight: 1000;
    color: #589F97;
    border-bottom: 1px solid #589F97;
    cursor: pointer;
  }

  .taskTitle {
    font-weight: 1000;
    font-size: 3rem;
    margin: 0rem 0 0.8rem 0;

  }

  .generalTask {
    margin: 3.5rem 0 3.5rem 0;
  }

  .choiceTask {
    margin-bottom: 3.5rem;
  }

  .download {
    margin: 0;
    margin-top: 0.5rem;
  }

  .titleCont {
    margin-top: 3.5rem;
  }

  p.download span {
    font-weight: 1000;
    color: #589F97;
    border-bottom: 1px solid #589F97;
    cursor: pointer;
    margin-bottom: 1rem;
    font-size: 1.2rem;
  }

  .editRegisterCont {
    margin-bottom: 8rem;
  }

  .submissionCont {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .subBoxCont {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .submissionBox {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: #A0D1CC;
    width: 34.5rem;
    padding: 20px;
    letter-spacing: 1px;
  }

  .subContent {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .lastEdit {
    color: #589F97;
    font-weight: 700;
    font-size: 1.2rem;
    margin: 0 0 0.3rem 0;
  }

  .dragDrop {
    color: #BBBBBB;
    font-weight: 1000;
    font-size: 1.5rem;
    margin: 0 0 0.3rem 0;
  }

  .editDownload {
    margin: 0 0 1rem 0;
    display: flex;
    flex-direction: row;
  }

  .clickDownload {
    margin: 0;
    font-weight: light;
    font-size: 12px;
    color: #589F97;
  }

  .clickUpload {
    margin: 0;
    font-weight: light;
    color: #BBBBBB;
  }

  .specialEdit {
    margin: 0;
    font-size: 12px;
  }

  .specialBrowse {
    margin: 0;
  }

  p.specialEdit span {
    margin: 0;
    color: #589F97;
    font-weight: 700;
    margin-left: 5px;
    border-bottom: 1px solid;
    cursor: pointer;
  }

  p.specialBrowse span {
    margin: 0;
    color: #BBBBBB;
    font-weight: 700;
    margin-left: 5px;
    border-bottom: 1px solid;
    cursor: pointer;
  }

  .descChoiceTask {
    margin: 2rem 0 1rem 0;
    font-size: 1.4rem;
  }

  .editSubBtn {
    background: #589F97;
    color: #FAFAFA;
    height: 2rem;
    width: 200px;
    cursor: pointer;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .subText {
    font-weight: 700;
    font-size: 0.9rem;
  }

  .submissionIcon {
    margin-bottom: 1rem;
  }

  .submissionIcon2 {
    margin-bottom: 1rem;
    padding-top: 1rem;
  }

  @media(max-width:1024px) {
    .container {
      padding: 4rem 3rem 1rem 3rem;
    }

    .submissionBox {
      width: 30rem;
      letter-spacing: 0px;
      padding: 12px;
    }
  }

  @media(max-width:768px) {
    letter-spacing: 0px;

    .container {
      padding: 4rem 2rem 1rem 2rem;
    }
    .titleCont {
      display: flex;
      flex-direction: row;
    }
    .taskTitle {
      margin-right: 1rem;
    }
    .submissionCont {
      flex-direction: column;
      margin-bottom: 3rem;
    }

    .descChoiceTask {
      font-size: 1.2rem;
    }

    .subBoxCont > div {
      width: 100%;
    }

    .submissionBox {
      width: 100%;
    }

    .taskTitle {
      font-size: 2rem;
    }

    .titleCont {
      margin-top: 1rem;
    }

    .vectorLine {
      margin: 1.5rem 0 1.5rem 0;
    }

    .generalTask {
      margin: 2.5rem 0 2.5rem 0;
    }
  }

  @media(max-width:728px) {
    .container {
      padding: 2rem 2rem 1rem 2rem;      
    }
  }

  @media(max-width:600px) {
    .container {
      padding: 2rem 1rem 1rem 1rem;
    }

    .deadlineText {
      letter-spacing: 1px;
    }

    .greeting {
      font-size: 2.5rem;
    }

    .notice {
      letter-spacing: 1px;
      font-size: 12px;
    }
  }

  @media(max-width: 480px) {
    .deadlineText {
      letter-spacing: 0px;
      font-size: 14px;
      margin-bottom: 0;
    }

    .taskTitle {
      font-size: 1.5rem;
      margin-right: 0.4rem;
    }

    p.download span {
      font-size: 1rem;
    }

    .descChoiceTask {
      font-size: 14px;
      margin: 0;
    }

    .titleCont {
      margin-top: 0;
    }
  
    .generalTask {
      margin-top: 1rem;
    }

    .submissionBox {
      padding-bottom: 1.5rem;
    }

    .submissionIcon img, .submissionIcon2 img {
      width: 10rem;
      padding-top: 1rem;
    }

    .lastEdit {
      font-size: 1rem;
    }

    .dragDrop {
      font-size: 1.2rem;
    }

    .clickUpload {
      font-size: 14px;
    }

    .specialBrowse {
      font-size: 14px;
    }

    .submissionIcon2 {
      padding-top: 0;
    }

  }

  @media(max-width:385px) {
    .greeting {
      letter-spacing: 1px;
      font-size: 2rem;
    }

    .registerNow {
      font-size: 1.1rem;
    }

    .taskTitle {
      font-size: 1.2rem;
    }

    .choiceTask {
      margin-bottom: 2rem;
    }

    .descChoiceTask {
      font-size: 14px;
    }

    p.download span {
      font-size: 0.9rem;
    }

    .submissionBox {
      padding-bottom: 14px;
    }

    .submissionIcon img, .submissionIcon2 img {
      padding-top: 0.5rem;
    }

    .lastEdit {
      letter-spacing: 0px;
      font-size: 14px;
    }

    .editSubBtn {
      width: 10rem;
    }

    .subText {
      font-size: 14px;
    }
  }
`;