import styled from 'styled-components';

export const TimeWidgetContainer = styled.div`

  font-family: 'Open Sans', sans-serif;
  letter-spacing: 7px;

  .timeWidget {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top: 2rem;
  }

  .boxCont {
    width: 20%;
  }

  .blackBox {
    height: 12rem;
    background: #222222;
    color: white;

    display: flex;
    justify-content: center;
    align-content: center;
  }

  .timeText {
    font-size: 8rem;
    font-weight: 1000;
    margin-top: 1.8rem;
  }

  @media(max-width:1024px) {
    .timeText {
      font-size: 5rem;
    }

    .blackBox {
      height: 9rem;
    }
  }

  @media(max-width:768px) {
    letter-spacing: 4px;

    .timeText {
      font-size: 4rem;
    }

    .blackBox {
      height: 8rem;
    }

    .descText {
      font-size: 12px;
    }
  }

  @media(max-width:600px) {
    .timeText {
      font-size: 3rem;
      margin-top: 1.5rem;
      margin-left: 0.4rem;
    }

    .blackBox {
      height: 6rem;
    }
  }

  @media(max-width:480px) {
    letter-spacing: 2px;
    .timeText {
      font-size: 2.4rem;
      margin-top: 1rem;
      margin-left: 0.2rem;
    }

    .blackBox {
      height: 4.5rem;
    }

    .descText {
      font-size: 10px;
      letter-spacing: 0px;
    }
  }

  @media(max-width:360px) {
    letter-spacing: 2px;
    .timeText {
      font-size: 2rem;
      margin-top: 1rem;
      margin-left: 0;
    }

    .boxCont {
      width: 22%;
    }
    .blackBox {
      height: 4.1rem;
    }
  }
`;
