import axios from 'axios';
import swal from 'sweetalert';
import { push } from 'react-router-redux';

import {
    UPLOAD_SUBMISSION,
    UPLOAD_SUBMISSION_SUCCESS,
    UPLOAD_SUBMISSION_FAILED,
    FETCH_SUBMISSION,
    FETCH_SUBMISSION_SUCCESS,
    FETCH_SUBMISSION_FAILED,
    EDIT_SUBMISSION,
    EDIT_SUBMISSION_SUCCESS,
    EDIT_SUBMISSION_FAILED,
    
} from './constants';

import { submissionApi, editSubmissionApi } from 'api';


export function uploadSubmission(data) {

  return (dispatch) => {
    dispatch({
        type: UPLOAD_SUBMISSION
    })

    axios.post(submissionApi, data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
  })
      .then((response) => {
        swal("Submit Success!", "Upload file submission berhasil", "success");
        dispatch({
          payload: response.data,
          type: UPLOAD_SUBMISSION_SUCCESS,
        })
      })
      .catch((err) => {
        swal("Submit Failed!", "Upload file submission gagal", "error");
        dispatch({
          payload: err.response,
          type: UPLOAD_SUBMISSION_FAILED
        })
      })
  }
}

export function editSubmission(data, subId) {

  return (dispatch) => {
    dispatch({
        type: EDIT_SUBMISSION
    })

    axios.patch(editSubmissionApi(subId), data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
  })
      .then((response) => {
        swal("Submit Success!", "Edit file submission berhasil", "success");
        dispatch({
          payload: response.data,
          type: EDIT_SUBMISSION_SUCCESS,
        })
      })
      .catch((err) => {
        swal("Submit Failed!", "Edit file submission gagal", "error");
        dispatch({
          payload: err.response,
          type: EDIT_SUBMISSION_FAILED
        })
      })
  }
}