/* eslint-disable sort-keys, radix, react/jsx-key, operator-linebreak, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { EnsiklopediaPageContainer } from './style';
import { DIVISI, PROSEDUR } from './data';
import EnsiklopediaAccordion from 'components/EnsiklopediaAccordion';

class EnsiklopediaPage extends React.Component {

  render() {

    return (
    <EnsiklopediaPageContainer>
      <div className="container">

        <div className="upperTitle">
          <h1 className="pageTitle">
            ENSIKLOPEDIA
          </h1>
        </div>

        <div className="content">

          <div className="leftContent">

            <div className="contentHeader">
              <h2 className="pageTitle">
                DIVISI
              </h2>
            </div>

            {DIVISI.map((res) => (<EnsiklopediaAccordion item={res}/>))}

          </div>

          <div className="rightContent">

            <div className="contentHeader">
              <h2 className="pageTitle">
                PROSEDUR & PERSYARATAN
              </h2>
            </div>

            {PROSEDUR.map((res) => (<EnsiklopediaAccordion item={res}/>))}

          </div>
        </div>

      </div>
    </EnsiklopediaPageContainer>
    );
  }
}

EnsiklopediaPage.propTypes = {
}

function mapStateToProps(state) {
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EnsiklopediaPage);
