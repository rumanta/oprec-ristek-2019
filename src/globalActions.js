/* eslint-disable sort-keys, valid-jsdoc */
/*
 * Actions describe changes of state in your application
 */
import auth from './auth';
import axios from 'axios';
import { push } from 'react-router-redux';

import {
  SET_AUTH,
  SET_USER,
  SENDING_REQUEST,
  REQUEST_ERROR,
  CLEAR_ERROR,
  SET_SERVER_TIME
} from './globalConstants';

import {
  FETCH_USER_PROFILE,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_FAILED,
} from 'containers/RegistrationPage/constants';

import { fetchServerTimeApi, profileApi } from 'api';

const userAuth = auth.loggedIn();

axios.defaults.headers.common.Authorization = `JWT ${userAuth.token}`;

export function login(user) {
  return (dispatch) => {
    dispatch(sendingRequest(true));

    dispatch(setUser(user));
    dispatch(setAuthState(true))

    auth.login(user);

    dispatch(sendingRequest(false))
    dispatch(push('/home'))
  }
}

export function logout() {
  return (dispatch) => {
    dispatch(sendingRequest(true));

    auth.logout();

    dispatch(setAuthState(false))
    dispatch(setUser({}));
    dispatch(sendingRequest(false));
  }
}

export function fetchServerTime() {
  return (dispatch) => {
    dispatch(sendingRequest(true));
    axios.get(fetchServerTimeApi)
    .then((response) => {
      dispatch(setServerTime(response.data.datetime));
    })
    .catch((error) => {
      dispatch(requestError(error.response));
    })
    .finally(
      dispatch(sendingRequest(false))
    );
  }
}

export function fetchUserProfile() {
  return (dispatch) => {
    dispatch(sendingRequest(true));

    dispatch({
      type: FETCH_USER_PROFILE
    })

    axios.get(profileApi)
    .then((response) => {
      dispatch({
        type: FETCH_USER_PROFILE_SUCCESS,
        payload: response.data
      })
    })
    .catch((error) => {
      dispatch({
        type: FETCH_USER_PROFILE_FAILED,
        payload: error.response
      })
    })
    .finally(
      dispatch(sendingRequest(false))
    );
  }
}

/**
 * Sets the authentication state of the application
 * @param  {boolean} newAuthState True means a user is logged in, false means no user is logged in
 */
export function setAuthState(newAuthState) {
  return {
    type: SET_AUTH,
    newAuthState
  };
}

export function setUser(user) {
  return {
    type: SET_USER,
    user
  };
}

/**
 * Sets the `currentlySending` state, which displays a loading indicator during requests
 * @param  {boolean} sending True means we're sending a request, false means we're not
 */
export function sendingRequest(sending) {
  return {
    type: SENDING_REQUEST,
    sending
  };
}

/**
 * Sets the `error` state to the error received
 * @param  {object} error The error we got when trying to make the request
 */
export function requestError(error) {
  return {
    type: REQUEST_ERROR,
    error
  };
}

/**
 * Sets the `error` state as empty
 */
export function clearError() {
  return { type: CLEAR_ERROR };
}

export function setServerTime(payload) {
  return {
    type: SET_SERVER_TIME,
    payload
  }
}

