import NotFoundPage from 'containers/NotFoundPage';
import LandingPage from 'containers/LandingPage';
import DashboardPage from 'containers/DashboardPage';
import EnsiklopediaPage from 'containers/EnsiklopediaPage';
import RegistrationPage from 'containers/RegistrationPage';

export const routes = [
  // {
  //   'component': LogoutModule,
  //   'exact': true,
  //   'path': '/logout'
  // },
  // {
  //   'component': PickSchedulePage,
  //   'exact': true,
  //   'path': '/susun'
  // },
  // {
  //   'component': ShowSchedulePage,
  //   'exact': true,
  //   'path': '/jadwal/:slug'
  // },
  // {
  //   'component': ShowSchedulePage,
  //   'exact': true,
  //   'path': '/jadwal'
  // },
  {
    'component': RegistrationPage,
    'exact': true,
    'path': '/form',
  },
  {
    'component': DashboardPage,
    'exact': true,
    'path': '/home',
  },
  {
    'component': LandingPage,
    'exact': true,
    'path': '/',
  },
  {
    'component': EnsiklopediaPage,
    'exact': true,
    'path': '/ensiklopedia',
  },
  { 'component': NotFoundPage }
];
