/* eslint-disable sort-keys, radix, init-declarations, react/jsx-key, object-property-newline, no-lonely-if, valid-jsdoc */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { LandingPageContainer } from './style';

import { login } from 'globalActions';
import { loginApi } from 'api';
import { isLoggedIn } from 'selectors';


let loginWindow;

class LandingPage extends React.Component {

  constructor() {
    super();
    this.receiveLoginData = this.receiveLoginData.bind(this);
  }

  componentDidMount() {
    window.addEventListener('message', this.receiveLoginData, false);

    if (this.props.isLoggedIn) {
      this.props.push('/home');
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn && !this.props.isLoggedIn) {
      this.props.push('/');
    }
  }

  onLogin() {
    loginWindow = window.open(loginApi, '_blank', 'toolbar=0,location=0,menubar=0');

    const getUserDataInterval = setInterval(() => {
      // stop interval when login window has closed
      if (loginWindow.closed) {
        clearInterval(getUserDataInterval);
      }

      // postMessage to the window, the message is not important whatsoever,
      // what important is that CP ORION get CP OMEGA origin window
      // so CP ORION can send message back to CP OMEGA
      loginWindow.postMessage('MadeByWebdev2019', loginApi);
    }, 1000);
  }

  /**
   *  Catch login data from message sent by CP ORION via postMessage function
   *  Caught by setting an event listener to 'message' event
   */
  receiveLoginData(event) {
    // For Chrome, the origin property is in the event.originalEvent object.
    const origin = event.origin || event.originalEvent.origin;
    const user = event.data;
    // MAKE SURE FUNCTION CALLER ORIGIN IS FROM CP ORION DOMAIN! SECURITY PURPOSES.
    if (loginApi.startsWith(origin)) {
      this.props.login(user);
      if (loginWindow) {
        loginWindow.close()
      }
    }
  }

  render() {

    return (
    <LandingPageContainer>
      <div className="landingCont">
        <div className="leftHand">
          <div className="leftCont">
            <h1 className="oprec-text">
              OPEN
            </h1>
            <h1 className="oprec-text">
              RECRUITMENT
            </h1>
            <p className="ristek-text">
              Anggota Ristek 2019
            </p>
          </div>
        </div>
        <div className="rightHand">
          <div className="rightCont">
            <h1 className="sso" onClick={this.onLogin}>
              <span>LOGIN SSO</span>
            </h1>
            <h1 className="sso2" onClick={() => this.props.push('/ensiklopedia')}>
              <span>ENSIKLOPEDIA</span>
            </h1>
          </div>
        </div>
      </div>
    </LandingPageContainer>
    );
  }
}

LandingPage.propTypes = {
  push: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.boolean,
}

function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    login: (user) => dispatch(login(user)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
