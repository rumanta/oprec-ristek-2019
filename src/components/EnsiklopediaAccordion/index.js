
import React from 'react';
import PropTypes from 'prop-types';

import { Accordion, Heading } from './style';
import ArrowIcon from 'assets/arrow.svg';

class EnsiklopediaAccordion extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();

    this.state = {
      expanded: false,
    };
  }

  toggle = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    const { expanded } = this.state;
    const { title, content } = this.props.item;

    return (
      <Accordion onClick={this.toggle}>
        <Heading expanded={expanded}>
          <h4>{title}</h4>
          <img src={ArrowIcon} alt="drop" />
        </Heading>

        {expanded && <p className="content">{content}</p>}

      </Accordion>
    );
  }
}

EnsiklopediaAccordion.propTypes = {
  item: PropTypes.object.isRequired,
};

export default EnsiklopediaAccordion;
