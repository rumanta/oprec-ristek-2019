 /* eslint-disable sort-keys, no-case-declarations, radix, react/jsx-key, operator-linebreak, object-property-newline, no-lonely-if */

import { fromJS } from 'immutable';
import { isEmpty } from 'lodash';

import {
    UPLOAD_SUBMISSION,
    UPLOAD_SUBMISSION_SUCCESS,
    UPLOAD_SUBMISSION_FAILED,
    FETCH_SUBMISSION,
    FETCH_SUBMISSION_SUCCESS,
    FETCH_SUBMISSION_FAILED,
} from './constants';

const initialState = fromJS({
    currentlySending: false,
    submission: null,
    errorSub: null,
  });

  function dashboardReducer(state = initialState, action) {
    switch (action.type) {
      case UPLOAD_SUBMISSION_SUCCESS:
        return state.set('submission', action.payload);
      case UPLOAD_SUBMISSION_FAILED:
        return state.set('errorSub', action.payload);
      case FETCH_SUBMISSION_SUCCESS:
        return state.set('submission', action.payload);
      case FETCH_SUBMISSION_FAILED:
        return state.set('errorSub', action.payload);

      default:
          return state;
      }
}

export default dashboardReducer;