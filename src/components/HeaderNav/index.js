/* eslint-disable sort-keys, radix, init-declarations, react/jsx-key, object-property-newline, no-lonely-if, valid-jsdoc */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { HeaderContainer } from './style';

import LogoSet from 'assets/logoset.svg';
import LogoSetBurger from 'assets/logoset_burger.svg';
import Burger from 'assets/burger.svg';
import Close from 'assets/close.svg';

import { loginApi } from 'api';
import { login, logout } from 'globalActions';
import { isLoggedIn, getUser } from 'selectors';

let loginWindow;

class HeaderNav extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false,
      isExpandedDesktop: false,
      name: '',
    };
    this.receiveLoginData = this.receiveLoginData.bind(this);
  }

  UNSAFE_componentWillMount() {
    document.addEventListener('mousedown', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false);
  }

  componentDidMount() {
    window.addEventListener('message', this.receiveLoginData, false);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn && !this.props.isLoggedIn) {
      this.props.push('/');
    }

    if (nextProps.user) {
      const name = this.trimUsername(nextProps.user.name)
      this.setState({
        name,
      })
    }
  }

  onLogin() {
    loginWindow = window.open(loginApi, '_blank', 'toolbar=0,location=0,menubar=0');

    const getUserDataInterval = setInterval(() => {
      // stop interval when login window has closed
      if (loginWindow.closed) {
        clearInterval(getUserDataInterval);
      }

      // postMessage to the window, the message is not important whatsoever,
      // what important is that CP ORION get CP OMEGA origin window
      // so CP ORION can send message back to CP OMEGA
      loginWindow.postMessage('MadeByWebdev2019', loginApi);
    }, 1000);
  }

  receiveLoginData(event) {
    // For Chrome, the origin property is in the event.originalEvent object.
    const origin = event.origin || event.originalEvent.origin;
    const user = event.data;
    // MAKE SURE FUNCTION CALLER ORIGIN IS FROM CP ORION DOMAIN! SECURITY PURPOSES.
    if (loginApi.startsWith(origin)) {
      this.props.login(user);
      if (loginWindow) {
        loginWindow.close()
      }
    }
  }

  onLogout() {
    const logoutWindow = window.open('https://sso.ui.ac.id/cas2/logout', '_blank', 'toolbar=0,location=0,menubar=0');
    const getUserDataInterval = setInterval(() => {
      // stop interval when login window has closed
      if (logoutWindow.closed) {
        clearInterval(getUserDataInterval);
      }

      // postMessage to the window, the message is not important whatsoever,
      // what important is that CP ORION get CP OMEGA origin window
      // so CP ORION can send message back to CP OMEGA
      logoutWindow.postMessage('MadeByWebdev2019', 'https://sso.ui.ac.id/cas2/logout');
    }, 1000);

    this.setState({
      isExpanded: false,
      isExpandedDesktop: false,
    })

    this.props.push('/')
    this.props.logout();
  }

  handleClick = (evt) => {
    if (this.node && this.btn) {
      if (!this.node.contains(evt.target) && !this.btn.contains(evt.target)) {
        this.setState({
          isExpandedDesktop: false
        });
      }
    }
  }

  movingPage = (route) => {
    this.props.push(route)
    this.setState({
      isExpandedDesktop: false,
      isExpanded: false,
    })
  }

  trimUsername = (name) => (
    name && name.split(' ')[0].toUpperCase()
  );

  renderExpansionNav = () => (
    <div ref={node => this.node = node} className="navContainer">
      <div className="navBox1" onClick={() => this.movingPage('/home')}>
        <p>DASHBOARD</p>
      </div>
      <div className="navBox2" onClick={() => this.onLogout()}>
        <p>LOGOUT</p>
      </div>
    </div>
  );

  toggleExpand() {
    const { isExpanded } = this.state;
    this.setState({ isExpanded: !isExpanded });
  }

  toggleExpandDesktop() {
    const { isExpandedDesktop } = this.state;
    this.setState({ isExpandedDesktop: !isExpandedDesktop });
  }

  render() {
    const loggedIn = this.props.isLoggedIn;
    const { isExpanded } = this.state;

    console.log('statee', this.state)

    return (
    <HeaderContainer>
      <div className={isExpanded ? "expand" : "hide"}>
        <div className="upperBurger">
          <img className="ristekLogoBurger" src={LogoSetBurger} onClick={() => this.movingPage('/home')}/>
          <img className="closeBurger" src={Close} onClick={() => this.toggleExpand()}/>
        </div>

        <div className="burgerTitle">
          <h1>MAIN MENU</h1>
        </div>

        <div className="burgerMenus">
          <div className="menu">
            <h3>ENSIKLOPEDIA</h3>
          </div>
          <div className="menu">
            <h3>DASHBOARD</h3>
          </div>
        </div>

      </div>
      <div className="headerBase">
        <img src={LogoSet} onClick={() => this.movingPage('/home')} className="ristekLogo"/>
        <img className="rightMenuBurger" src={isExpanded ? Close : Burger} onClick={() => this.toggleExpand()}/>

        <div className="containerRight">
          <div className="rightHeader">
            <div onClick={() => this.movingPage('/ensiklopedia')} className="leftButton">ENSIKLOPEDIA</div>
            {
              loggedIn ?
              <div ref={btn => this.btn = btn} onClick={() => this.toggleExpandDesktop()} className="rightButton">{this.state.name ? this.state.name : this.trimUsername(this.props.user.name)}</div>
              :
              <div className="rightButton" onClick={() => this.onLogin()}>LOGIN SSO</div>
            }
          </div>
          {this.state.isExpandedDesktop && this.renderExpansionNav()}
        </div>
      </div>
    </HeaderContainer>
    );
  }
}

HeaderNav.propTypes = {
  push: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  user: PropTypes.shape(),
}

function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
    user: getUser(state),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    login: (user) => dispatch(login(user)),
    logout: () => dispatch(logout()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderNav);
