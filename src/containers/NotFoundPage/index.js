import React from 'react';
import NotFoundIcon from 'assets/404.svg';
import { NotFoundPageStyle } from './style';


export default class NotFoundPage extends React.Component {
  render() {
    return (
        <NotFoundPageStyle>

          <div className="fourOFourContainer">

            <div className="leftContent">
              <img src={NotFoundIcon} />
            </div>

            <div className="rightContent">
              <div className="centerContent">
                <h1 className="lost">ARE YOU LOST?</h1>
                <p className="goHome">Go Home please</p>
              </div>
            </div>

          </div>

        </NotFoundPageStyle>
    );
  }
}
