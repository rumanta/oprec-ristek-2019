import styled from 'styled-components';

export const Accordion = styled.button`
  width: 100%;
  transition: 0.25s ease all;
  margin: 1rem 0 0;
  padding: 0;
  outline: none;
  cursor: pointer;
  border-bottom: 0.5px solid #DDDDDD;
  font-family: 'Open Sans', sans-serif;

  &:hover,
  &:focus {
    outline: none;
  }

  .content {
    text-align: left;
    font-size: 12px;
  }

`;

export const Heading = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  padding: 0.5rem 0;

  h4 {
    margin: 0 1rem 0 0;
    flex: 1;
    font-size: 1rem;
    font-weight: 600;
    text-align: left;
  }

  img {
    height: 0.75rem;
    width: auto;
    transform: rotate(${(props) => props.expanded ? '180deg' : '0deg'});
    transition: 0.5s ease all;
  }
`;
