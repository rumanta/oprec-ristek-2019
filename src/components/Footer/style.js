import styled from 'styled-components';

export const FooterContainer = styled.div`

  position: absolute;
  bottom: 0;
  width: 100%;
  padding-left: 4rem;
  padding-right: 4rem;
  padding-bottom: 1rem;
  font-family: 'Proxima Nova', sans-serif;

  .footerContainer {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  p {
    margin: 0;
  }

  .conquer {
  }

  .leftContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .leftFooter {
    display: flex;
    flex-direction: row;
  }

  .rightFooter {
    display: flex;
    flex-direction: row;
  }

  .linkRistek {
    margin-right: 1.2rem;
    text-align: right;
  }

  .footerIcon {
    width: 8rem;
  }

  @media(max-width:728px){
    & {
      padding: 0rem 1rem 1rem 1rem;
    }

    .linkRistek {
      display: none;
    }

    .conquer, .footerIcon {
      width: 7rem;
    }
  }

`;
